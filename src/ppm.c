//Molotilov Nikita, Group 210
#include <stdlib.h>
#include <stdio.h>

#include "ari.h"
#include "ppm.h"

struct Table{
    int value;
    int aggression;
    struct Table *next;
    int last_access;
};

#define PPM_LEVEL 5
static const int weight[PPM_LEVEL+1] = {1, 1, 3, 10, 30, 200};

struct Context{
    unsigned char chars[PPM_LEVEL];
    int num;
};

static const int AGGRESSION_SCALE = 1000000;
static const int MAX_TABLE = 50000;

struct Table *InitTablePpm(int num){
    struct Table *table = malloc(257 * sizeof(struct Table));
    for (int i = 0; i <= 256; i++){
        if (num == 0) {
            table[i].value = 10 * i;
        } else {
            table[i].value = 0;
        }
        table[i].aggression = AGGRESSION_SCALE / 200;
        table[i].next = NULL;
        table[i].last_access = 0;
    }
    return table;
}

void DestroyTablePpm(struct Table *table){
    for (int i = 0; i <= 256; i++){
        if (table[i].next != NULL){
            DestroyTablePpm(table[i].next);
        }
    }
    free(table);
}

void CompressTablePpm(struct Table *table){
    unsigned int c1 = 0;
    unsigned int c2;
    for (int i = 1; i <= 256; i++){
        c2 = table[i].value;
        table[i].value = table[i-1].value + (table[i].value - c1 + 1) / 2;
        c1 = c2;
    }
}

static const int TABLE_LIFETIME = 200000;

void UpdateTablePpm(struct Table *table, unsigned char c, struct Context *p_context, int time){
    int scale = 0;
    for (int j = 0; j <= p_context->num; j++) {
        long long change = (long long)table[c].aggression * (table[256].value + scale) / AGGRESSION_SCALE;
        for (int i = 0; i <= 256; i++) {
            if (i > c){
                table[i].value += change;
            }
            if (table[i].next != NULL && table[i].last_access - time > TABLE_LIFETIME){
                DestroyTablePpm(table[i].next);
                table[i].next = NULL;
            }
        }
        if (table[256].value >= MAX_TABLE) {
            CompressTablePpm(table);
        }
        scale += table[256].value * weight[j];
        if (table[p_context->chars[j]].next == NULL && j < p_context->num) {
            table[p_context->chars[j]].next = InitTablePpm(j+1);
        }
        table[p_context->chars[j]].last_access = time;
        table = table[p_context->chars[j]].next;
    }
    for (int i = PPM_LEVEL - 1; i > 0; i--){
        p_context->chars[i] = p_context->chars[i-1];
    }
    if (p_context->num < PPM_LEVEL){
        p_context->num++;
    }
    p_context->chars[0] = c;
}

void CalculateEffectiveTable(struct Table *table, struct Table *effective_table, struct Context context){
    for (int j = 0; j <= 256; j++){
        effective_table[j].value = 0;
    }
    for (int i = 0; table != NULL; i++){
        for (int j = 0; j <= 256; j++){
            effective_table[j].value += table[j].value * weight[i];
        }
        table = table[context.chars[i]].next;
    }
}

#define INTERVAL_RANGE 4294967295
static const unsigned int FIRST_QTR = INTERVAL_RANGE/4 + 1;
static const unsigned int HALF = (INTERVAL_RANGE/4 + 1) * 2;
static const unsigned int THIRD_QTR = (INTERVAL_RANGE/4 + 1) * 3;

void CompressPpm(FILE *ifp, FILE *ofp){
    struct Buff buff;
    buff.num = 0;

    struct Table *table = InitTablePpm(0);
    struct Table *effective_table = malloc(257 * sizeof(struct Table));
    struct Context context;
    context.num = 0;

    unsigned int l = 0;
    unsigned int h = INTERVAL_RANGE;
    int bits_follow = 0;
    unsigned char c;

    unsigned int size;
    WriteSize(ifp, ofp, &size);

    for (; size > 0; size--){
        CalculateEffectiveTable(table, effective_table, context);
        fread(&c, 1, 1, ifp);
        unsigned long long range = (unsigned long long) h - l + 1;
        h = l + effective_table[c+1].value * range / effective_table[256].value - 1;
        l = l + effective_table[c].value * range / effective_table[256].value;
        for(;;){
            if (h < HALF){
                WriteBitsFollow(&buff, 0, bits_follow, ofp);
                bits_follow = 0;
            } else if (l >= HALF){
                l -= HALF;
                h -= HALF;
                WriteBitsFollow(&buff, 1, bits_follow, ofp);
                bits_follow = 0;
            } else if ((l >= FIRST_QTR) && (h < THIRD_QTR)){
                bits_follow++;
                l -= FIRST_QTR;
                h -= FIRST_QTR;
            } else {break;}
            l += l;
            h += h + 1;
        }
        UpdateTablePpm(table, c, &context, size);
    }
    if (l != 0 || bits_follow != 0) {
        WriteBitsFollow(&buff, 1, bits_follow, ofp);
    }
    if (buff.num > 0) {
        buff.value <<= (8 - buff.num);
        if (buff.value != 0) {
            fwrite(&(buff.value), 1, 1, ofp);
        }
    }
}

void DecompressPpm(FILE *ifp, FILE *ofp){
    struct Buff buff;
    buff.num = 0;

    struct Table *table = InitTablePpm(0);
    struct Table *effective_table = malloc(257 * sizeof(struct Table));;
    struct Context context;
    context.num = 0;

    unsigned int l = 0;
    unsigned int h = INTERVAL_RANGE;
    unsigned char c;

    int size;
    fread(&size, 1, sizeof(int), ifp);

    unsigned int value = ValueInit(ifp);

    for(; size > 0; size--){
        CalculateEffectiveTable(table, effective_table, context);
        unsigned long long range = (unsigned long long) h - l + 1;
        for (c = 0; l + effective_table[c+1].value * range / effective_table[256].value - 1 < value; c++);
        h = l + effective_table[c+1].value * range / effective_table[256].value - 1;
        l = l + effective_table[c].value * range / effective_table[256].value;
        for(;;){
            if (h < HALF) {;
            } else if (l >= HALF){
                l -= HALF;
                h -= HALF;
                value -= HALF;
            } else if ((l >= FIRST_QTR) && (h < THIRD_QTR)){
                l -= FIRST_QTR;
                h -= FIRST_QTR;
                value -= FIRST_QTR;
            } else {break;}
            l += l;
            h += h + 1;
            value += value + ReadBit(&buff, ifp);
        }
        fwrite(&c, 1, 1, ofp);
        UpdateTablePpm(table, c, &context, size);
    }
}

void compress_ppm(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    CompressPpm(ifp, ofp);

    fclose(ifp);
    fclose(ofp);
}

void decompress_ppm(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    DecompressPpm(ifp, ofp);

    fclose(ifp);
    fclose(ofp);
}
