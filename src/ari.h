#pragma once

void compress_ari(char *ifile, char *ofile);
void decompress_ari(char *ifile, char *ofile);

void WriteSize(FILE *ifp, FILE *ofp, unsigned int *p_size);
struct Buff{
    unsigned char value;
    int num;
};
void WriteBit (struct Buff *p_buff, int bit, FILE *ofp);
void WriteBitsFollow(struct Buff *p_buff, int bit, int follow, FILE *ofp);
unsigned int ValueInit(FILE *ifp);
int ReadBit(struct Buff *p_buff, FILE *ifp);