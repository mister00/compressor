#include <stdio.h>

int main(){
	FILE *ifp = (FILE *)fopen("test_8", "rb");
	FILE *cfp = (FILE *)fopen("test_8a", "rb");
	char a;
	char b;
	int i = 0;
	while (!feof(ifp)){
		fread(&a, 1, 1, ifp);
		fread(&b, 1, 1, cfp);
		if (a != b){
			printf("%d", i);
			break;
		}
		i++;
	}
	return 0;
}
