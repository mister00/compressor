#include <stdlib.h>
#include <stdio.h>

#include "ari.h"

void WriteSize(FILE *ifp, FILE *ofp, unsigned int *p_size){
    fseek(ifp, 0, SEEK_END);
    *p_size = ftell(ifp);
    rewind(ifp);
    fwrite(p_size, 1, sizeof(int), ofp);
}

void WriteBit (struct Buff *p_buff, int bit, FILE *ofp){
    p_buff->value <<= 1;
    p_buff->value += bit;
    p_buff->num++;
    if (p_buff->num == 8){
        fwrite(&(p_buff->value), 1, 1, ofp);
        p_buff->num = 0;
    }
}

void WriteBitsFollow(struct Buff *p_buff, int bit, int follow, FILE *ofp){
    WriteBit(p_buff, bit, ofp);
    for (; follow > 0; follow--){
        WriteBit(p_buff, !bit, ofp);
    }
}

unsigned int ValueInit(FILE *ifp){
    unsigned int value = 0;
    for (int i = 1; i <= 4; i++) {
        unsigned char byte;
        int ok;
        ok = fread(&byte, 1, 1, ifp);
        if (ok == 0){
            byte = 0;
        }
        value = value * 256 + byte;
    }
    return value;
}

int ReadBit(struct Buff *p_buff, FILE *ifp){
    if (p_buff->num == 0){
        int ok;
        ok = fread(&(p_buff->value), 1, 1, ifp);
        if (ok == 0){
            p_buff->value = 0;
        }
    }
    unsigned char bit = p_buff->value & 128;
    bit >>= 7;
    p_buff->value <<= 1;
    p_buff->num++;
    if (p_buff->num == 8) {
        p_buff->num = 0;
    }
    return bit;
}

struct Table{
    int value;
    int aggression;
};

static const int AGGRESSION_SCALE = 1000000;

struct Table *InitTable(int mode){
    struct Table *table = malloc(257 * sizeof(struct Table));
    for (int i = 0; i <= 256; i++){
        table[i].value = 10*i;
        table[i].aggression = AGGRESSION_SCALE / 50;
    }
    return table;
}

static const int MAX_TABLE = 100000;

void CompressTable(struct Table *table){
    int c1 = 0;
    int c2;
    for (int i = 1; i <= 256; i++){
        c2 = table[i].value;
        table[i].value = table[i-1].value + (table[i].value - c1 + 5) / 2;
        c1 = c2;
    }
}

void UpdateTable(struct Table *table, unsigned char c){
    long long change = (long long)table[0].aggression * table[256].value / AGGRESSION_SCALE;
    for (int i = c+1; i <= 256; i++){
        table[i].value += change;
    }
    if (table[256].value >= MAX_TABLE){
        CompressTable(table);
    }
}

void ChangeTables(struct Table *table, struct Table *table2){
    for (int i = 0; i <= 256; i++){
        table[i].value = table2[i].value;
    }
    table[0].aggression = table2[0].aggression;
}

void CompareTables(struct Table *table, struct Table *table2){
    unsigned long long sum_diff = 0;
    for (int i = 0; i <= 255; i++){
        long long diff = llabs(((long long)table[i+1].value - table[i].value) * table2[256].value - ((long long)table2[i+1].value - table2[i].value) * table[256].value);
        diff /= table[256].value;
        sum_diff += diff * diff;
        //if (diff > sum_diff){
         //   sum_diff = diff;
        //}
    }
    //sum_diff /= (long long)table2[256].value;// * table[256].value;
    if (sum_diff >= (long long)table2[256].value * table2[256].value / 5){;// * table[256].value * 2){
        ChangeTables(table, table2);
    }
    table[0].aggression -= table[0].aggression / 500;
    if (table[0].aggression < AGGRESSION_SCALE / 700){
        table[0].aggression = AGGRESSION_SCALE / 700;
    }
}

#define INTERVAL_RANGE 4294967295
static const unsigned int FIRST_QTR = INTERVAL_RANGE/4 + 1;
static const unsigned int HALF = (INTERVAL_RANGE/4 + 1) * 2;
static const unsigned int THIRD_QTR = (INTERVAL_RANGE/4 + 1) * 3;

void CompressAri(FILE *ifp, FILE *ofp){
    struct Buff buff;
    buff.num = 0;

    struct Table *table = InitTable(1);
    struct Table *table2 = InitTable(2);

    unsigned int l = 0;
    unsigned int h = INTERVAL_RANGE;
    int bits_follow = 0;
    unsigned char c;

    unsigned int size;
    WriteSize(ifp, ofp, &size);

    for (; size > 0; size--){
        fread(&c, 1, 1, ifp);
        unsigned long long range = (unsigned long long) h - l + 1;
        h = l + table[c+1].value * range / table[256].value - 1;
        l = l + table[c].value * range / table[256].value;
        for(;;){
            if (h < HALF){
                WriteBitsFollow(&buff, 0, bits_follow, ofp);
                bits_follow = 0;
            } else if (l >= HALF){
                l -= HALF;
                h -= HALF;
                WriteBitsFollow(&buff, 1, bits_follow, ofp);
                bits_follow = 0;
            } else if ((l >= FIRST_QTR) && (h < THIRD_QTR)){
                bits_follow++;
                l -= FIRST_QTR;
                h -= FIRST_QTR;
            } else {break;}
            l += l;
            h += h + 1;
        }
        UpdateTable(table, c);
        UpdateTable(table2, c);
        CompareTables(table, table2);
    }
    if (l != 0 || bits_follow != 0) {
        WriteBitsFollow(&buff, 1, bits_follow, ofp);
    }
    if (buff.num > 0) {
        buff.value <<= (8 - buff.num);
        if (buff.value != 0) {
            fwrite(&(buff.value), 1, 1, ofp);
        }
    }
}

void DecompressAri(FILE *ifp, FILE *ofp){
    struct Buff buff;
    buff.num = 0;

    struct Table *table = InitTable(1);
    struct Table *table2 = InitTable(2);

    unsigned int l = 0;
    unsigned int h = INTERVAL_RANGE;
    unsigned char c;

    int size;
    fread(&size, 1, sizeof(int), ifp);

    unsigned int value = ValueInit(ifp);

    for(; size > 0; size--){
        unsigned long long range = (unsigned long long) h - l + 1;
        for (c = 0; l + table[c+1].value * range / table[256].value - 1 < value; c++);
        h = l + table[c+1].value * range / table[256].value - 1;
        l = l + table[c].value * range / table[256].value;
        for(;;){
            if (h < HALF) {;
            } else if (l >= HALF){
                l -= HALF;
                h -= HALF;
                value -= HALF;
            } else if ((l >= FIRST_QTR) && (h < THIRD_QTR)){
                l -= FIRST_QTR;
                h -= FIRST_QTR;
                value -= FIRST_QTR;
            } else {break;}
            l += l;
            h += h + 1;
            value += value + ReadBit(&buff, ifp);
        }
        fwrite(&c, 1, 1, ofp);
        UpdateTable(table, c);
        UpdateTable(table2, c);
        CompareTables(table, table2);
    }
}

void compress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    CompressAri(ifp, ofp);

    fclose(ifp);
    fclose(ofp);
}

void decompress_ari(char *ifile, char *ofile) {
    FILE *ifp = (FILE *)fopen(ifile, "rb");
    FILE *ofp = (FILE *)fopen(ofile, "wb");

    DecompressAri(ifp, ofp);

    fclose(ifp);
    fclose(ofp);
}
